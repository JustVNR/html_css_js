function loaded(callable) {
    window.addEventListener('DOMContentLoaded', callable);
}

function loadNav(callback = null) {
    fetch('navbar.html')
        .then(function (response) {
            return response.text();
        })
        .then(
            function (nav) {
                //document.querySelector('body > nav').innerHTML = nav;
                // document.querySelector('nav').innerHTML = nav;
                document.body.insertAdjacentHTML("afterbegin", nav)
                callback();
            }
        )
        .catch(function (error) {
            console.error(error);
        });

    //console.log('navbar chargée');
}

loaded(function () {
    loadNav(function () { });
});